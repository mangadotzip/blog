---
title: "Resume"
draft: false
categories: 
  - CV
  - Resume
  - Career Development
tags:
  - Cybersecurity
  - SQA Engineer
  - Tech Skills
---

## Jonathan Weems  -- [Resume](https://gitlab.com/mangadotzip/blog/-/blob/master/content/Weems_Resume.pdf?ref_type=heads)

**Phone:** (650) 429-8121 

**Email:** [weems@weems.blog](mailto:weems@weems.blog)

### **Cybersecurity Professional**  
Dedicated to fortifying data privacy through vigilant monitoring and risk analysis. Expert in identifying vulnerabilities in fast-paced environments. Resourceful problem-solver with experience in software design, testing, and maintenance to safeguard digital assets and infrastructure. Detail-oriented leader committed to staying current with cybersecurity threats through continuous learning and training.

---

### **Areas of Expertise**
- Vulnerability Assessment  
- Security Standards & Compliance  
- Incident Response  
- System Security  
- Process Improvement  
- Risk Management  
- Technical Troubleshooting  
- Project Management  
- Systems Engineering  

---

### **Technical Proficiencies**
**Cloud Computing:** Amazon Web Services (AWS)  
**Programming Languages:** Java, Python, SQL  
**Frameworks:** HIPAA, SDLC, Agile Methodologies  
**Additional Skills:** Atlassian, Jira, NetSuite, Bitbucket, CI/CD Deployment  

---

### **Professional Experience**

#### **Sycle LLC** – Birmingham, AL (Remote)  
**Lead SQA Engineer** *(06/2014 – Current)*  
- Implemented Agile methodologies to eliminate errors and maximize customer satisfaction.  
- Enhanced deployment frequency and reduced downtime by improving toolsets and operations.  
- Conducted regression testing and documented test cases for robust project management.  
- Achieved cost savings of $10K+ by preventing and resolving software defects.  

**Level II Tech Support Engineer** *(06/2013 – 06/2014)*  
- Resolved technical defects across diverse systems, ensuring high customer satisfaction.  
- Monitored operations, utilizing diagnostic tools for quick error resolution.  
- Educated end-users on security policies, enhancing awareness of threats like malware and phishing.  
- Managed Tier II requests via NetSuite, ensuring effective issue resolution.  

#### **Mississippi College** – Computer Services Student Technician  
- Provided IT support, including PC installations, phone tech support, and system imaging.  
- Help Desk Management: Assisted in managing the help desk ticketing system, ensuring timely resolution of technical support requests, troubleshooting issues related to operating systems, network connectivity, and productivity software.
- System Maintenance & Upgrades: Assisted in the routine maintenance of campus computers, including software updates, system configurations, and installations of operating systems and applications.
- Network Support: Helped maintain and troubleshoot the campus network infrastructure, including wired and wireless connectivity issues, ensuring reliable and efficient internet access across the university.
---

### **Education**
- **Pursuing Bachelor of Science (BS), Cybersecurity & Information Assurance** *(Expected May 2026)*  
  Western Governor’s University  
- **Bachelor of Science (BS), Computer & Information Systems**  
  Mississippi College  

---
