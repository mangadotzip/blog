---
title: "Running a Honeypot on a Raspberry Pi 5: A Cybersecurity Learning Project"
date: 2025-02-01T12:00:00-06:00
draft: false
tags: ["Raspberry Pi", "Cybersecurity", "Honeypot", "Threat Intelligence", "ELK"]
categories: ["Security", "Projects", "Hands-On"]
description: "Learn how to set up a Raspberry Pi 5 honeypot using Cowrie and ELK to collect and analyze attack data. Gain hands-on cybersecurity experience for your portfolio."
---

## Introduction  

If you're looking to break into cybersecurity, hands-on experience is invaluable. A great way to develop security skills is by setting up a honeypot—an intentionally vulnerable system designed to attract attackers and log their behavior. In this guide, I’ll walk through how to set up a Raspberry Pi 5 as a honeypot, collect and analyze attack data, and use it to enhance your cybersecurity knowledge.  

## Why Use a Raspberry Pi 5?  

The Raspberry Pi 5 offers improved performance, making it an excellent, low-cost option for running security projects. With its quad-core ARM processor and up to 8GB RAM, it can handle lightweight security monitoring tools while consuming minimal power.  

## Tools and Setup  

We'll use **Cowrie**, an SSH/Telnet honeypot, to capture unauthorized login attempts, and **ELK (Elasticsearch, Logstash, Kibana)** to analyze and visualize the data.  

### 1. Install Raspberry Pi OS  

Flash **Raspberry Pi OS Lite** onto a microSD card using **Raspberry Pi Imager**. Boot up the Pi and configure network access.  

```bash
sudo apt update && sudo apt upgrade -y
```

### 2. Install Cowrie  

Cowrie simulates a real SSH/Telnet server and logs attacker activity.  

```bash
sudo apt install git python3-venv -y
git clone https://github.com/cowrie/cowrie.git
cd cowrie
python3 -m venv cowrie-env
source cowrie-env/bin/activate
pip install -r requirements.txt
```

Configure Cowrie by editing `cowrie.cfg`:  

```bash
cp cowrie.cfg.dist cowrie.cfg
nano cowrie.cfg
```

Enable SSH mode and change the listening port.  

Start Cowrie:  

```bash
./start.sh
```

### 3. Capture and Analyze Logs  

Cowrie logs all interactions in `~/cowrie/var/log/cowrie/cowrie.json`. To monitor live connections:  

```bash
tail -f ~/cowrie/var/log/cowrie/cowrie.log
```

For deeper analysis, set up ELK to visualize attacker activity.  

### 4. Install ELK Stack  

Install and configure **Elasticsearch**, **Logstash**, and **Kibana** to store and analyze attack data.  

```bash
sudo apt install elasticsearch logstash kibana -y
```

Set up Logstash to parse Cowrie logs and send data to Elasticsearch.  

```bash
sudo nano /etc/logstash/conf.d/cowrie.conf
```

Example configuration:  

```plaintext
input {
  file {
    path => "/home/pi/cowrie/var/log/cowrie/cowrie.json"
    start_position => "beginning"
    codec => "json"
  }
}
output {
  elasticsearch {
    hosts => ["http://localhost:9200"]
    index => "cowrie-%{+YYYY.MM.dd}"
  }
}
```

Start ELK services and access Kibana at `http://<your-pi-ip>:5601`.  

### 5. Report on Findings  

Once your honeypot is live, analyze the logs and create reports on:  

- **Who** is attempting to connect (IP addresses, geolocation).  
- **Where** the attacks originate from.  
- **What** methods attackers use (brute-force, credential stuffing, exploitation attempts).  

Use Kibana dashboards to visualize trends. You might see login attempts with common passwords or repeated probes from specific regions.  

## Conclusion  

Setting up a Raspberry Pi 5 honeypot is a fantastic way to gain hands-on cybersecurity experience. You can showcase this project in your portfolio, demonstrating skills in **threat intelligence, log analysis, and security operations**—all highly valuable in cybersecurity roles.  