---
title: "Hello World 2024"
date: 2024-07-01T22:05:01-05:00
draft: false
---

Many things have changed since the beginning of the year. We have seen several Health Orgs that have had serious Ransomware Infections that have had widespread impact. From Optum / UHC where Millions of Patients had their PHI Released and for weeks patients were not able to get needed medication and/or services, and providers were not able to get paid. And on top of that Ascension Healthcare, a major owner of hospitals in the United States also had a ransomware infection, friends who worked at an Ascension owned facility reported that they had to go back to using paper instead of their infected computer systems. We need widespread awareness of how ransomware infects computers, how users are able to click on a link in an email to become infected, etc., What an irony it is that computer malware is able to spread in an environment which tries to prevent Human to Human viral infections from spreading. We need to become as good at preventing the spread of Human to Computer Viruses.