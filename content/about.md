---
title: "About"
date: 2023-08-20T00:03:53-05:00
draft: false
---
My name is Michael Weems. I am a SQA Engineer passionate about information security and privacy. Everyone is using the internet, and everyone is using software on the internet. Thus, everyone should be able to use the internet with security and privacy so that they can trust the platforms they use and that their messages to friends and family are safe from third party interception. This also includes platform policies regarding trust and safety, tech policy including content moderation has become a interest of mine over the past decade. 

