# Contributing to Michael Weems' Blog

Welcome, and thank you for your interest in contributing to my personal blog! This document outlines the guidelines for contributing, whether you’re reporting an issue, suggesting a feature, or submitting a post.

---

## Contact Information

If you have any questions or need clarification, feel free to reach out:  
**Email:** [weems@weems.blog](mailto:weems@weems.blog)

---

## How You Can Contribute

### 1. **Suggest Topics**
   If you have ideas for blog posts that align with my themes (e.g., cybersecurity, data privacy, offensive security, or AWS learning), feel free to email your suggestions.

### 2. **Report Issues**
   Found a typo, broken link, or other problem? Please:
   - **Email:** Send a detailed description to [weems@weems.blog](mailto:weems@weems.blog) with the subject line: `Blog Issue Report`.
   - Include the page URL and description of the problem.

### 3. **Submit Guest Posts**
   Interested in writing for the blog? I’d love to feature your content!  
   Follow these steps to submit a guest post:
   - Email your draft as a Markdown file to [weems@weems.blog](mailto:weems@weems.blog).
   - Include a brief author bio (50-100 words) with your preferred byline and a link to your website/socials.
   - Ensure your post aligns with the blog's focus areas.

   #### **Guest Post Guidelines**
   - Posts should be **original** content, not published elsewhere.
   - Keep the tone conversational and professional.
   - Suggested word count: **800–1500 words**.
   - Use Markdown for formatting (headers, lists, links, etc.).

---

## Technical Contributions

If you're familiar with Hugo and Git, you can contribute to the blog directly by:
1. **Forking the Repository**  
   (If I make the source code public.)
2. **Making Changes**  
   Fix typos, improve formatting, or add functionality (e.g., themes or plugins).
3. **Submitting a Pull Request**  
   Please include a clear description of the changes and why they’re beneficial.

---

## Code of Conduct

All contributors are expected to follow these guidelines:  
- Be respectful and constructive.  
- Provide helpful feedback or suggestions.  
- Avoid spam, plagiarism, or offensive content.

---

Thank you for contributing to Michael Weems' Blog! Your input helps make this blog better for everyone.  
– Michael
